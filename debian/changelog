golang-github-allegro-bigcache (3.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh and update fix-32-bit.patch.
  * Switch Standards-Version to 4.7.0 (no changes needed).
  * Update debian/copyright years.

 -- Guillem Jover <gjover@sipwise.com>  Fri, 30 Aug 2024 18:31:14 +0200

golang-github-allegro-bigcache (2.2.5-5) unstable; urgency=medium

  * Update gitlab-ci.yml from its new refactored template.
  * Update debian/watch file.
  * Switch Standards-Version to 4.6.0 (no changes needed).
  * Use dh-sequence-golang instead of dh-golang and --with=golang.
  * Move location of license in Debian systems into a Comment field.
  * Update copyright years.

 -- Guillem Jover <gjover@sipwise.com>  Tue, 15 Mar 2022 22:16:18 +0100

golang-github-allegro-bigcache (2.2.5-4) unstable; urgency=medium

  * Mark -dev package as Multi-Arch:foreign

 -- Guillem Jover <gjover@sipwise.com>  Fri, 22 Jan 2021 18:57:01 +0100

golang-github-allegro-bigcache (2.2.5-3) unstable; urgency=medium

  * Fix atomic usage on 32-bit systems.
  * Exclude server code, which we do not need.

 -- Guillem Jover <gjover@sipwise.com>  Wed, 30 Dec 2020 16:01:14 +0100

golang-github-allegro-bigcache (2.2.5-2) unstable; urgency=medium

  * Update gitignore files

 -- Guillem Jover <gjover@sipwise.com>  Mon, 28 Dec 2020 16:04:03 +0100

golang-github-allegro-bigcache (2.2.5-1) unstable; urgency=medium

  * Initial release (Closes: #953658)

 -- Guillem Jover <gjover@sipwise.com>  Fri, 11 Dec 2020 23:43:15 +0100
